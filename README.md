Some checkpoints as below:

1. replace AndroidManifest.xml: <meta-data android:name="api_key" android:value="rnfju2FqM5Xjt5MnrulpbMtu" /> with your app api_key
2. check permission inside AndroidManifest.xml
3. check AndroidManifest.xml register/receive message Receiver
4. check push service config in AndroidManifest.xml: start from line 60 to line 102
5. your app Receiver should extends to PushMessageReceiver
6. Android 8.0 issue fix: refer to PushDemoActivity.java line 113 to 140
7. In some android device, you might need to set app auto start
8. In some android device, some app might intercept push notification to your app,need to turn off intercept and send push to your app
9. login to baidu push and send push from dashboard to do testing
10. if not receive, try kill app from process and test again